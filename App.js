import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Vibration,
  Dimensions,
  ToastAndroid
} from "react-native";
import BackgroundGeolocation from "@mauron85/react-native-background-geolocation";
import RNShake from "react-native-shake";
import { WebView } from "react-native-webview";
const { width, height } = Dimensions.get("window");
import KeyEvent from "react-native-keyevent";

var counters = 0;

export default class App extends Component<Props> {
  constructor() {
    super();
    this.state = {
      background: false,
      nextShakeTime: 0,
    };
    /*  RNShake.addEventListener("ShakeEvent", () => {
      if (Date.now() > this.state.nextShakeTime)
        if (this.state.background) {
          Vibration.vibrate(10);
          BackgroundGeolocation.stop();
          this.setState({
            background: false,
            nextShakeTime: Date.now() + 2000
          });
        } else {
          BackgroundGeolocation.start();
          Vibration.vibrate(500);
          this.setState({
            background: true,
            nextShakeTime: Date.now() + 2000
          });
        }
    }); */
  }
  componentWillMount() {
    KeyEvent.onKeyDownListener(keyEvent => {
        counters++
        if (counters < 5){
        ToastAndroid.show(
          "Volume down button pressed" + counters + " times",
          ToastAndroid.SHORT
        );
        }
      if (counters === 5) {
        counters = 0;
        ToastAndroid.show(
          "Background tracking is ON",
          ToastAndroid.SHORT
        );
        BackgroundGeolocation.start();
        this.setState({background:true})
        Vibration.vibrate(40);
      }
      console.log(counters);
      setTimeout(() => {
        counters = 0;
      }, 3000);
    });

    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      notificationTitle: "Background tracking",
      notificationText: "enabled",
      notificationsEnabled: false,
      debug: false,
      startOnBoot: false,
      startForeground: false,
      stopOnTerminate: false,
      locationProvider: BackgroundGeolocation.RAW_PROVIDER,
      interval: 10000,
      fastestInterval: 5000,
      activitiesInterval: 10000,
      stopOnStillActivity: false
    });
    BackgroundGeolocation.on("location", location => {
      BackgroundGeolocation.startTask(taskKey => {
        const loc = [location.latitude, location.longitude];
        fetch("https://trackingserverszabi.herokuapp.com/places", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            coordinates: loc
          })
        });
        BackgroundGeolocation.endTask(taskKey);
      });
    });
  }
  componentDidMount() {}
  componentWillUnmount() {
    RNShake.removeEventListener("ShakeEvent");
  }

  render() {
    return (
      <View
        style={[
          styles.container,
          { backgroundColor: this.state.background ? "green" : "red" }
        ]}
      >
        <WebView
          source={{ uri: "https://martonszabolcs.hu" }}
          style={{ marginTop: 20, width: width, height: height * 0.8 }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "gray"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
