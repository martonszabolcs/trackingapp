package com.trackingapp;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.github.kevinejohn.keyevent.KeyEventPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.clipsub.RNShake.RNShakeEventPackage;
import com.marianhello.bgloc.react.BackgroundGeolocationPackage;
import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new KeyEventPackage(),
            new RNCWebViewPackage(),
            new RNShakeEventPackage(),
            new BackgroundGeolocationPackage(),
            new RNGeocoderPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
